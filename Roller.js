module.exports = {
  calculateFormula: function(formula) {
    let result = parseFormula(formula);
    return result;
  }
};

function walk(valStack, opStack, close) {
  while (opStack.length > 0 && valStack.length > 0) {
    if (opStack[opStack.length - 1] == "(") {
      if (close) {
        opStack.pop();
      }
      break;
    }
    let op = opStack.pop();
    let rhs = 0;
    let lhs = 0;

    let rhsObject = valStack.pop();
    if (typeof rhsObject == "object") {
      rhs = rhsObject.roll();
    } else {
      rhs = parseInt(rhsObject);
    }

    let lhsObject = valStack.pop();
    if (typeof lhsObject == "object") {
      lhs = lhsObject.roll();
    } else {
      lhs = parseInt(lhsObject);
    }

    let res = 0;
    switch (op) {
      case "+":
        res = lhs + rhs;
        break;
      case "-":
        res = lhs - rhs;
        break;
      case "*":
        res = lhs * rhs;
        break;
      case "/":
        res = lhs / rhs;
        break;
    }
    valStack.push(res);
  }
}

function parseFormula(formula) {
  let formulaParts = formula.split(/([+*-/()])/);

  let valStack = [];
  let opStack = [];

  let doFetch = true;
  for (let i = 0; i < formulaParts.length; i++) {
    let part = formulaParts[i];
    if (part == "(") {
      opStack.push(part);
    } else if (part == ")") {
      walk(valStack, opStack, true);
    } else {
      if (doFetch) {
        if (part.indexOf("d") != -1) {
          valStack.push(new Die(part.split("d")[0], part.split("d")[1]));
        } else {
          valStack.push(part);
        }
      } else {
        if (part == "+" || part == "-") {
          walk(valStack, opStack, false);
        }
        opStack.push(part);
      }
      doFetch = !doFetch;
    }
  }

  walk(valStack, opStack, false);
  let val = valStack.pop();
  if (typeof val == "object") {
    val = val.roll();
  }

  return val;
}
