# Die Roller

A project to create a dice roller in many languages.

## Todo List

- [x] C#
- [x] JS
- [x] Python
- [ ] Go
- [ ] Java
- [ ] Haskell
- [ ] C++
- [ ] php
- [ ] Ruby
- [ ] SQL
- [ ] COBOL
- [ ] Scala
- [ ] Batch
- [ ] Brainfuck

### Bots

- [x] Discord - Javascript - "@Dice Bot" Makes use of https://discord.js.org/#/docs/main/stable/general/welcome
- [x] Slack
- [x] Mastadon - @diebot@botsin.space