class Die {
  constructor(count, sides) {
    this.count = count;
    this.sides = sides;
  }

  roll() {
    let result = 0;

    for (let i = 0; i < this.count; i++) {
      result += Math.floor(Math.random() * this.sides) + 1;
    }

    return result;
  }
}

global.Die = Die;
