const Discord = require("discord.js");
const client = new Discord.Client();
const Roller = require("./Roller.js");
require("./Die.js");

let bot_settings = {
  tts: true
};

const env = require("dotenv");
env.config();

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on("message", msg => {
  if (msg.author != client.user) {
    console.log("----------------------");
    console.log(
      `New message recieved from ${msg.author.username} at ` + new Date()
    );
    console.log(`Message: ${msg.cleanContent}`);

    if (msg.isMemberMentioned(client.user)) {
      console.log(`${client.user.username} was mentioned`);
      if (msg.content.indexOf("roll") > -1) {
        let formula = msg.content
          .toLowerCase()
          .split("roll")[1]
          .replace(/(<([^>]+)>)/gi, "");
        console.log(`Formula to parse: ${formula}`);
        try {
          let result = Roller.calculateFormula(formula);
          console.log(`Result: ${result}`);
          replyRoll(formula, result, msg);
        } catch (e) {
          console.log(e.message);
          replyError();
        }
      } else if (msg.content.toLowerCase().indexOf("quiet") > -1) {
        bot_settings.tts = false;
        replyGeneric(msg, "Being quiet now!");
      } else if (msg.content.toLowerCase().indexOf("speak up") > -1) {
        bot_settings.tts  = true;
        replyGeneric(msg, "Being loud now!");
      } else if (msg.content.toLowerCase().indexOf("thank you") > -1) {
        react(msg, "\u{1F499}");
        replyGeneric(msg, `You're welcome`);
      } else {
        replyError(msg);
      }
    }
  }
});

client.login(process.env.BOTTOKEN);

function react(msg, emoji) {
  msg.react(emoji);
}

function replyRoll(formula, result, msg) {
  let reply_msg = `${formula} is ${result}`;
  msg
    .reply(reply_msg, bot_settings)
    .then(sent => console.log(`Sent a reply to ${msg.author.username}`))
    .catch(console.error);
}

function replyGeneric(msg, response) {
  msg
    .reply(response, bot_settings)
    .then(sent => console.log(`Sent a reply to ${msg.author.username}`))
    .catch(console.error);
}

function replyError(msg) {
  msg.reply(`I'm sorry but I do not understand your message. Try asking me to roll 1d20.`, bot_settings)
  .then(sent => console.log(`Sent a reply to ${msg.author.username}`))
  .catch(console.error);
}